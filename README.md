# Talana #

Prueba Técnica de aplicación para empleo.

## Introducción ##

La aplicación trata de un concurso básico en el cual los usuarios se van a poder inscribir sin tener la necesidad de estar registrados en el sistema, cuando se genere la inscripción de manera asíncrona el usuario recibirá en su bandeja de correo electrónico una invitación para formalizar dicha inscripción (activarla), además de solicitarle que también formalice su registro en el sistema, es importante que este registro se lleve a cabo pues el ganador del concurso debe estar registrado en el sistema. 

**Detalles Técnicos**

* La aplicación debe contener Apis rest full para ser consumidas con VueJs o ReactJs.
* Las Apis deben tener validaciones.
* se debe generar una api en la cual se premie a un participante siempre y cuando este ya haya formalizado la inscripción y se haya registrado dentro del sistema.

## Version ##
V.1.0

## Modelado de datos ##

Para esta aplicación se pensó en el diseño de 3 tablas, Participante, Concurso e Inscripción, a continuación se detallan cada una de estas:

### Participante ###

| Campo | Tipo | Descripción |
|----- | ----- | -------- |
| id   | int PK | Clave primaria, Identificador unico de cada Participante |
| first_name   | varchar | Nombre de cada Participante |
| last_name   | varchar | Apellido de cada Participante |
| email   | varchar unique | email de cada Participante |
| created_by   | int FK | Identificador del usuario que registra dicho participante |

### Concurso ###

| Campo | Tipo | Descripción |
|----- | ----- | -------- |
| id   | int PK | Clave primaria, Identificador unico de cada Participante |
| name   | varchar | Nombre de cada Participante |
| start_date   | varchar | Apellido de cada Participante |
| end_date   | varchar unique | email de cada Participante |
| winner   | int FK | Identificador del participante registrado (usuario) que gano dicho concurso |
| created_by   | int FK | Identificador del usuario que registra dicho participante |

### Inscripción ###

| Campo | Tipo | Descripción |
|----- | ----- | -------- |
| id   | int PK | Clave primaria, Identificador unico de cada Participante |
| concurso   | int FK | Identificador del concurso |
| participante   | int FK | Identificador del participante |
| status   | boolean | Flag que me permite saber si la inscripción fué activada a través del email |
| created_by   | int FK | Identificador del usuario que registra dicho participante |

## URL's o Rutas ##

Las urls de esta aplicación se encuentran dentro de la app prueba en el directorio `prueba/urls/__init__.py`, usted verá el siguiente código:

```python
# prueba/urls/__init__.py
from django.urls import include, path

from prueba.urls.apis import router

app_name = "prueba"

urlpatterns = [
    path("api/v1/", include(router.urls)),
    path("jwt/", include("prueba.urls.jwt", namespace="jwt")),
]
```

A simple vista se puede observar que se esta trabajando de manera modularizada, en el primer `path` usted verá como se realiza la invocación al las urls del `router` del `drf` y en el segundo usted verá la invocación a las vistas de autenticación de `json web token (jwt)`

### Directorio de apis ###

*Participante*

| Acción | Método | Ruta |
|--- | ---- | ----- |
| Listar | GET | `/prueba/api/v1/participantes` |
| Detallar | GET | `/prueba/api/v1/participantes/{pk}/` |
| Crear | POST | `/prueba/api/v1/participantes` |
| Editar | UPDATE - PATCH | `/prueba/api/v1/participantes/{pk}/` |
| Eliminar | DELETE | `/prueba/api/v1/participantes/{pk}/` |

___

*Concurso*

| Acción | Método | Ruta |
|--- | ---- | ----- |
| Listar | GET | `/prueba/api/v1/concursos` |
| Detallar | GET | `/prueba/api/v1/concursos/{pk}/` |
| Seleccionar ganador del concurso | GET | `/prueba/api/v1/concursos/{pk}/select_winner/` |
| Crear | POST | `/prueba/api/v1/concursos` |
| Inscribir participantes al concurso | POST | `/prueba/api/v1/concursos/{pk}/inscripcion_participante/` |
| Editar | UPDATE - PATCH | `/prueba/api/v1/concursos/{pk}/` |
| Eliminar | DELETE | `/prueba/api/v1/concursos/{pk}/` |

___

*Inscripción*

| Acción | Método | Ruta |
|--- | ---- | ----- |
| Listar | GET | `/prueba/api/v1/inscripciones` |
| Detallar | GET | `/prueba/api/v1/inscripciones/{transaction_id}/` |
| Activar | GET | `/prueba/api/v1/inscripciones/{transaction_id}/activar` |
| Crear | POST | `/prueba/api/v1/inscripciones` |
| Editar | UPDATE - PATCH | `/prueba/api/v1/inscripciones/{transaction_id}/` |
| Eliminar | DELETE | `/prueba/api/v1/inscripciones/{transaction_id}/` |

___


## Viewsets ##

Los viewsets en conjunto con los serializadores son los encargados de generar la lógica deseada para que las apis tengan el comportamiento esperado y deseado, en algunos viewsets como concurso o inscripción nos dimos a la tarea de agregar un par de acciones que nos permiten hacer uso de las cualidades de la programación orientada a objetos (POO), no se creo ninguna estrategia para el borrado o la eliminación de registros pues no se mencionó nada en el documento rector de la prueba.

Los serializadores como tal definen la logica y las validaciones para cada modelo, es importante mencionar que al igual que las urls la aplicación cuenta con un sistema de modularización con el fin de hacer del app una estructura de código fácil de mantener y de dar soporte.

```python
# prueba/viewsets/inscripcion.py

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from prueba.models import Inscripcion
from prueba.serializers import InscripcionSerializer
from utils import IsSystemInternalPermission


class InscripcionViewSet(ModelViewSet):
    queryset = Inscripcion.objects.all()
    serializer_class = InscripcionSerializer
    lookup_field = "transaction_id"
    lookup_url_kwarg = "transaction_id"
    permission_classes = [IsSystemInternalPermission | IsAuthenticated]

    @action(detail=True, methods=["get"])
    def activar(self, request, *args, **kwargs):
        inscripcion = self.get_object()
        inscripcion.status = True
        inscripcion.save()
        return Response(
            {
                "msg": "La Inscripción ha sido activada"
            },
            status=status.HTTP_200_OK
        )


```

## Tareas celery ##

Las tareas celery son una parte fundamental de esta aplicación, las mismas estan dispuesta en la app core, dentro de core tendremos un folder que contiene la carpeta tasks y dentro de esta una carpeta con el nombre de la app a la cual pertenece, estando dentro de este directorio podrás observar que cada tarea tiene asignado un script, esto se maneja así para brindar soporte efectivo.

Ejemplo de ruta:

- send_mail: `prueba/core/tasks/prueba/send_mail.py`, en esta ruta usted podrá encontrar la función python `complete_inscription` la cual le envia un correo informando quew debe activar la inscripción a través de un link.

### Ejemplo de la tarea Celery ###

```python
# prueba/core/tasks/prueba/send_mail.py
from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.urls import reverse

from prueba.models import Inscripcion
from prueba.token_generator import JWTokenExternalUser

User = get_user_model()

logger = get_task_logger(__name__)


@shared_task
def complete_inscription(**kwargs):
    current_site = kwargs.pop("current_site")
    transaction_id = kwargs.pop("transaction_id")

    try:
        inscription = Inscripcion.objects.get(transaction_id=transaction_id)
    except Inscripcion.DoesNotExist:
        msg = f"=== NO EXISTE LA INSCRIPCION CON EL TRANSACTION ID\
            {transaction_id} ==="
        logger.info(msg)
        print(msg)
        return False
    else:

        participante = inscription.participante
        url = reverse(
            "prueba:inscripcion-activar",
            kwargs={"transaction_id": transaction_id},
        )
        form_action = f"{current_site}{url}"
        url_redirect = f"{current_site}/"
        print(form_action)
        contexto_email = {
            "py_name": settings.PY_NAME,
            "first_name": participante.first_name,
            "last_name": participante.last_name,
            "email": participante.email,
            "form_action": form_action,
            "url": url_redirect
        }

        jwt = JWTokenExternalUser()
        jwt.encode_token(str(participante.transaction_id), settings.PY_NAME)

        template = get_template("home.html")
        content = template.render(contexto_email)

        email = EmailMultiAlternatives(
            "Correo Talana!!!",
            "Talana!!!",
            settings.EMAIL_HOST_USER,
            [participante.email],
        )

        email.attach_alternative(content, "text/html")
        email.send()
        print("REVISA TU BANDEJA DE ENTRADA")
        logger.info("Enviado el mail")
        return True


```