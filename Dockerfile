FROM python:3.9.5
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
RUN mkdir /code/logs
WORKDIR /code

COPY . /code/
COPY requirements.txt /code/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

CMD ["gunicorn", "-c", "bin/gunicorn/conf.py", "--bind", ":8000", "config.wsgi:application"]