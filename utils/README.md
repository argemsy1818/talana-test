# BLKSOFT SUBMODULE #
<p style="text-align: justify;">A modo de introducción, este repositorio es un compendio de códigos y conocimientos adquiridos a lo largo de la maravillosa travesía por el mundo de la programación, de muchos compañeros con los cuales compartí y a los cuales agradezco por permitirme tomar de ellos las mejores prácticas, su conocimiento y me tomaría el atrevimiento de decir, colaborar en la fortificación de los mismo.
</p>

<br/>

## Índice ##
___

## Agregar este submódulo en cualquier proyecto Django

```#!/bin/bash

git submodule add https://argenis1818@bitbucket.org/argenis1818/blksoft_package.git utils
git commit -m "Agregando el submodulo blksoft package."
git push


# Siempre que esté clonando un repositorio de Git que tenga submódulos, debe ejecutar un comando adicional para que se extraigan los submódulos.
* git submodule update --init --recursive

# Comandos para actualizar un submódulo de Git
* git submodule update --remote --merge
```

<table style="width:100%">
    <thead>
        <tr>
            <th>Contenido</th><th class="text-center">Descripción</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Modelo Base</td>
            <td class="text-center">Modelo abstracto del cual deben heredar todos y cada uno de los modelos del sistema.</td>
        </tr>
        <tr>
            <td>Administrador Base</td>
            <td class="text-center">Modelo abstracto del cual deben heredar todos y cada uno de los modelos del sistema.</td>
        </tr>
        <tr>
            <td>JWTView</td>
            <td class="text-center">Modelo abstracto del cual deben heredar todos y cada uno de los modelos del sistema.</td>
        </tr>
        <tr>
            <td>Pagination</td>
            <td class="text-center">Modelo abstracto del cual deben heredar todos y cada uno de los modelos del sistema.</td>
        </tr>
    </tbody>
</table>
