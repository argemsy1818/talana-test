from django.http import HttpResponseForbidden
from django.template import TemplateDoesNotExist, loader


def permission_denied(request, exception, template_name='403.html'):
    """
    Permission denied (403) handler.

    Templates: :template:`403.html`
    Context: None

    If the template does not exist, an Http403 response containing the text
    "403 Forbidden" (as per RFC 7231) will be returned.
    """
    try:
        template = loader.get_template(template_name)
    except TemplateDoesNotExist:
        if template_name != '403.html':
            # Reraise if it's a missing custom template.
            raise
        return HttpResponseForbidden(
            '<h1>403 Forbidden</h1>', content_type='text/html'
            )
    return HttpResponseForbidden(
        template.render(request=request, context={'exception': str(exception)})
    )