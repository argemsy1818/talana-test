# -*- coding: utf-8 -*-
# import os
# import django
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.base")
# django.setup()


from .admin import BaseAdmin
from .backends import EmailAuthBackend
from .cache import get_cache, set_cache
from .db_router import RouterDB
from .decorator_class import class_decorator
from .get_model_fields import get_model_fields
from .jwt_mixin import JWTView
from .models import BaseModel
from .normalize_text import normalize_text
from .pagination import LargeResultsSetPagination, LinkHeaderPagination
from .permissions import IsSystemInternalPermission
from .validate_uuid import validate_uuid4

__all__ = [
    "BaseModel",
    "EmailAuthBackend",
    "LinkHeaderPagination",
    "LargeResultsSetPagination",
    "IsSystemInternalPermission",
    "normalize_text",
    "BaseAdmin",
    "JWTView",
    "RouterDB",
    "validate_uuid4",
    "get_model_fields",
    "get_cache",
    "set_cache",
    "class_decorator"
]