"""Decorador de clase

Este decorador aplica a toda la clase en General

"""


def class_decorator():
    def wrap(cls):
        class ModelViewSet(cls):

            def dispatch(self, request, *args, **kwargs):

                return super().dispatch(request, *args, **kwargs)
        return ModelViewSet
    return wrap
