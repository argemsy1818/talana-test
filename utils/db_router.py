# Clase que maneja las acciones de guardado y
# lectura en la base de datos rest


class RouterDB(object):

    def __init__(self, database="saturno_bd", label="pruebas"):
        self.bd = database
        self.label = label

    def db_for_read(self, model, **hints):
        "Point all operations on rest models to 'saturno'"
        if model._meta.app_label == self.label:
            return self.db
        return 'default'

    def db_for_write(self, model, **hints):
        "Point all operations on rest models to 'saturno'"
        if model._meta.app_label == self.label:
            return None
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):

        "Allow any relation if a both models in saturno app"
        if obj1._meta.app_label == self.label and obj2._meta.app_label == self.label:
            return True
        # Allow if neither is saturno app
        elif self.label not in [obj1._meta.app_label, obj2._meta.app_label]:
            return True
        return False

    def allow_syncdb(self, db, model):
        if db == self.db or model._meta.app_label == self.label:
            return False
        else:
            return True