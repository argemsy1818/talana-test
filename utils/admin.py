from django.contrib import admin, messages


class BaseAdmin(admin.ModelAdmin):
    date_hierarchy = 'pub_date'
    readonly_fields = ("transaction_id",)
    actions = ['change_status']
    using = 'default'

    def save_model(self, request, obj, form, change):
        # Tell Django to save objects to the 'other' database.
        if not self.using:
            self.using = request.database
        obj.save(using=self.using)

    def delete_model(self, request, obj):
        # Tell Django to delete objects from the 'other' database
        if not self.using:
            self.using = request.database
        obj.delete(using=self.using)

    def get_queryset(self, request):
        # Tell Django to look for objects on the 'other' database.
        if not self.using:
            self.using = request.database
        return super().get_queryset(request).using(self.using)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # Tell Django to populate ForeignKey widgets using a query
        # on the 'other' database.
        if not self.using:
            self.using = request.database
        return super().formfield_for_foreignkey(
            db_field, request, using=self.using, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        # Tell Django to populate ManyToMany widgets using a query
        # on the 'other' database.
        if not self.using:
            self.using = request.database
        return super().formfield_for_manytomany(
            db_field, request, using=self.using, **kwargs)

    def change_status(self, request, queryset):
        if not self.using:
            self.using = request.database
        if hasattr(self.model, 'is_active'):
            model = self.model._meta.verbose_name
            n = queryset.using(self.using).count()
            message = f"{n} {model} cambiadas."
            self.message_user(request, message, messages.SUCCESS)
            for item in queryset:
                if item.is_active:
                    item.is_active = False
                else:
                    item.is_active = True
                item.save()
        return queryset
    change_status.short_description = 'Cambiar status'
