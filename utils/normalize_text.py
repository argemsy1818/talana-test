import re
from unicodedata import normalize


def normalize_text(get_text):
    """
    Parsea las cadenas de texto eliminando espacios, comillas, tildes,
    y todo tipo de elementos que dificulten su legibilidad en base de
    datos
    """
    text = str(get_text)
    new_text = re.sub(
            r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
            normalize("NFD", text), 0, re.I
        )
    return new_text.strip()