from typing import List

from django.db.models import Model


def get_model_fields(model: Model) -> List[str]:
    """ Retorna un listado con los campos de cualquier modelo """
    listado = model._meta.fields
    return list(map(lambda x: x.name, listado))
