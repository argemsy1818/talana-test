from typing import Any

from django.core.cache import cache

TIME_OUT = 60 * 120


def set_cache(key: str, value: Any):
    """ GUARDAR EN CACHE VARIABLES DE CUALQUIER TIPO """
    cache.set(key, value, timeout=TIME_OUT)


def get_cache(key: str, default: str):
    """ RETORNA LA CLAVE DEL DICCIONARIO ALMACENADO EN CACHÉ """
    try:
        return cache.get(key)
    except KeyError:
        return cache.get(default)
