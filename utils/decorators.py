"""
Decorador de métodos  
"""


def decorator__method(method):
    def add_logic(request, *args, **kwargs):
        
        return method(request, *args, **kwargs)

    return add_logic
