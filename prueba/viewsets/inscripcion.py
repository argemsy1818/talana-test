from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from prueba.models import Inscripcion
from prueba.serializers import InscripcionSerializer
from utils import IsSystemInternalPermission


class InscripcionViewSet(ModelViewSet):
    queryset = Inscripcion.objects.all()
    serializer_class = InscripcionSerializer
    lookup_field = "transaction_id"
    lookup_url_kwarg = "transaction_id"
    permission_classes = [IsSystemInternalPermission | IsAuthenticated]

    @action(detail=True, methods=["get"])
    def activar(self, request, *args, **kwargs):
        inscripcion = self.get_object()
        inscripcion.status = True
        inscripcion.save()
        return Response(
            {
                "msg": "La Inscripción ha sido activada"
            },
            status=status.HTTP_200_OK
        )
