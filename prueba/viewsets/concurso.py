from core.tasks.prueba.send_mail import User
from django.contrib.sites.shortcuts import get_current_site
from django.db import IntegrityError, transaction
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from core.tasks import complete_inscription
from prueba.models import Concurso, Inscripcion, Participante
from prueba.serializers import ConcursoSerializer, ParticipanteSerializer
from random import randint
from datetime import datetime
from django.utils import timezone


class ConcursoViewSet(ModelViewSet):
    queryset = Concurso.objects.all()
    serializer_class = ConcursoSerializer

    def validate_participante(self, email: str):
        existe: bool = False
        msg: str = ""
        self.object = self.get_object()

        inscripcion = Inscripcion.objects.filter(
            concurso=self.object, participante__email=email
        )
        if inscripcion.exists():
            existe = True
            msg = {"msg": "Usted ya se encuentra registrado en este concurso"}

        return existe, msg

    @transaction.atomic
    def createParticipante(self, **data):
        """Create Participante Instance if this no exists on Concurso"""
        participante, created = Participante.objects.get_or_create(
            email=data.get("email")
        )
        if created:
            participante.first_name = data.get("first_name")
            participante.last_name = data.get("last_name")
            participante.email = data.get("email")
            participante.created_by = self.request.user
            participante.save()
        return participante

    @action(detail=True, methods=["post"])
    def inscripcion_participante(self, request, *args, **kwargs):
        data = request.POST
        try:
            data["email"]
            data["first_name"]
            data["last_name"]
        except KeyError as e:
            return Response(
                {"msg": f"Se espera la clave {str(e)} en los datos."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        else:
            email = data["email"]
            first_name = data["first_name"]
            last_name = data["last_name"]
            existe, msg = self.validate_participante(email)
            if existe:
                estado = status.HTTP_400_BAD_REQUEST
            else:
                # se crea el participante
                options = {
                    "email": email,
                    "first_name": first_name,
                    "last_name": last_name,
                }
                participante = self.createParticipante(**options)
                # se crea la nueva inscripción
                inscripcion = Inscripcion()
                try:
                    with transaction.atomic():
                        inscripcion.participante = participante
                        inscripcion.concurso = self.get_object()
                        inscripcion.created_by = request.user
                except IntegrityError as e:
                    return Response(
                        {
                            "msg": (
                                "Error mientras se estaba\
                                generando la inscripción"
                            ),
                            "sError": str(e),
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )
                else:
                    inscripcion.save()
                    msg = {
                        "msg": (
                            "Éxito, ha registrado un nuevo participante,\
                                    revise su bandeja de entrada y activa\
                                    esta inscripción."
                        ),
                    }
                    estado = status.HTTP_201_CREATED
                    # ejecuto la tarea celery
                    current_site = get_current_site(request)
                    complete_inscription(
                        current_site=current_site,
                        transaction_id=inscripcion.transaction_id,
                    )

        return Response(msg, status=estado)

    @action(detail=True, methods=["get"])
    def select_winner(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.status == 0:
            inscripciones = Inscripcion.objects.filter(
                concurso=self.object, status=True
            ).exclude(participante=None)
            if inscripciones.exists():
                emails_participantes = inscripciones.values("participante__email")
                usuarios = User.objects.filter(email__in=emails_participantes)
                if not usuarios.exists():
                    return Response(
                        {
                            "msg": (
                                "Los usuarios inscritos para este curso no han\
                                formalizado aún su registro en la plataforma."
                            )
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )
                else:
                    nro_usuarios = usuarios.count() - 1
                    nro_aleatorio = randint(0, nro_usuarios)
                    winner = usuarios[nro_aleatorio]
                    try:
                        with transaction.atomic():
                            self.object.winner = winner
                            self.object.status = 2
                            self.object.end_date = timezone.now()
                    except IntegrityError as e:
                        return Response(
                            {
                                "msg": "Ha ocurrido un error al momento de guardar.",
                                "sError": str(e),
                            }
                        )
                    else:
                        self.object.save()
                    concurso = self.object.name

                    print("Notificar via email que ganó el concurso!!!")

                    return Response(
                        {
                            "msg": (
                                f"El ganador del concurso {concurso}\
                                es: {winner.email}"
                            )
                        },
                        status=status.HTTP_200_OK,
                    )
            else:
                return Response(
                    {
                        "msg": (
                            "Upss! lo sentimos, los participantes de este concurso\
                            no han activado aún la inscripción."
                        )
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )
        elif self.object.status == 2:
            return Response(
                {
                    "msg": "Concurso Finalizado",
                    "resumen": {
                        "ganador": self.object.winner.email,
                        "cierre": self.object.end_date.strftime("%d-%m-%Y %H:%M:%S"),
                    },
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        elif self.object.status == 1:
            return Response(
                {"msg": "El concurso ha sido pospuesto de manera temporal"},
                status=status.HTTP_400_BAD_REQUEST,
            )

    def get_serializer_class(self):
        if self.action == "inscripcion_participante":
            return ParticipanteSerializer
        return super().get_serializer_class()
