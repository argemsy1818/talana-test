from rest_framework.viewsets import ModelViewSet

from prueba.models import Participante
from prueba.serializers import ParticipanteSerializer


class ParticipanteViewSet(ModelViewSet):
    queryset = Participante.objects.all()
    serializer_class = ParticipanteSerializer
