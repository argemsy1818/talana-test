from prueba.admin.concurso import ConcursoAdmin
from prueba.admin.inscripcion import InscripcionAdmin
from prueba.admin.participante import ParticipanteAdmin

__all__ = [
    "ConcursoAdmin",
    "InscripcionAdmin",
    "ParticipanteAdmin",
]