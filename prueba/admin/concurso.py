from django.contrib import admin

from prueba.models import Concurso
from utils import BaseAdmin


@admin.register(Concurso)
class ConcursoAdmin(BaseAdmin):
    using = "default"
    list_display = ("name", "start_date", "end_date", "winner")