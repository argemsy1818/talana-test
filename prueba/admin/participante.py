from django.contrib import admin

from prueba.models import Participante
from utils import BaseAdmin


@admin.register(Participante)
class ParticipanteAdmin(BaseAdmin):
    using = "default"
    list_display = ("id", "first_name", "last_name", "email")