from django.contrib import admin

from prueba.models import Inscripcion
from utils import BaseAdmin


@admin.register(Inscripcion)
class InscripcionAdmin(BaseAdmin):
    using = "default"
    list_display = ("concurso", "participante", "status")