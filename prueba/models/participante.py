from django.contrib.auth import get_user_model
from django.db import models

from utils import BaseModel

User = get_user_model()
# Create your models here.


class Participante(BaseModel):
    first_name: str = models.CharField(
        max_length=255,
        verbose_name="Nombre *",
        help_text="varchar(255) required")
    last_name: str = models.CharField(
        max_length=255,
        verbose_name="Apellido *",
        help_text="varchar(255) required")
    email: str = models.EmailField(
        max_length=255,
        unique=True,
        db_index=True,
        verbose_name="Email *",
        help_text="varchar(255) required")
    created_by: int = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="Creado por",
        help_text="INT (FK), required",
        related_name="participantes")

    class Meta:
        ordering = ["-id"]
        verbose_name = "Participante"
        verbose_name_plural = "Participantes"

    def __str__(self) -> str:
        return f"{self.email}"