from django.contrib.auth import get_user_model
from django.db import models

from utils import BaseModel

User = get_user_model()
# Create your models here.


class Concurso(BaseModel):
    name: str = models.CharField(
        max_length=255,
        verbose_name="Nombre del concurso*",
        help_text="varchar(255) required",
    )
    start_date: str = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name="Fecha Inicio *",
        help_text="varchar(255) required",
    )
    end_date: str = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name="Fecha Cierre *",
        help_text="varchar(255) required",
    )
    winner: int = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        verbose_name="Ganador",
        help_text="INT (FK), no required",
        related_name="concursos_winners",
    )
    created_by: int = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="Creado por",
        help_text="INT (FK), required",
        related_name="concursos",
    )
    status: int = models.SmallIntegerField(
        choices=((0, "Activo"), (1, "Postergado"), (2, "Finalizado")),
        default=0,
        verbose_name="Status del concurso",
        help_text="INT",
    )

    class Meta:
        ordering = ["-id"]
        verbose_name = "Concurso"
        verbose_name_plural = "Concursos"

    def __str__(self) -> str:
        return f"{self.name}"
