from prueba.models.concurso import Concurso
from prueba.models.inscripcion import Inscripcion
from prueba.models.participante import Participante

__all__ = [
    "Concurso",
    "Inscripcion",
    "Participante",
]