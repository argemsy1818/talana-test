from django.contrib.auth import get_user_model
from django.db import models

from utils import BaseModel

User = get_user_model()
# Create your models here.


class Inscripcion(BaseModel):

    concurso: int = models.ForeignKey(
        "prueba.Concurso",
        on_delete=models.CASCADE,
        verbose_name="Concurso *",
        help_text="INT (FK), required",
    )

    participante: int = models.ForeignKey(
        "prueba.Participante",
        on_delete=models.CASCADE,
        verbose_name="Participante *",
        help_text="INT (FK), required",
    )
    status: bool = models.BooleanField(
        default=False,
        verbose_name="Status",
        help_text=("boolean default 0,\
                    indica de la inscripción está activa o no"),
    )
    created_by: int = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="Creado por",
        help_text="INT (FK), required",
        related_name="inscripciones")

    class Meta:
        ordering = ["-id"]
        verbose_name = "Inscripción"
        verbose_name_plural = "Inscripciones"
        unique_together = ["concurso", "participante"]

    def __str__(self) -> str:
        print("---")
        status_dict = {
            True: "Si",
            False: "No"
        }
        status = status_dict[self.status]
        return f"{self.concurso}: {self.participante} | Status: {status}"
