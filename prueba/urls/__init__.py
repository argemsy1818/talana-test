from django.urls import include, path

from prueba.urls.apis import router

app_name = "prueba"

urlpatterns = [
    path("api/v1/", include(router.urls)),
    path("jwt/", include("prueba.urls.jwt", namespace="jwt")),
]