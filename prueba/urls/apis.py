from rest_framework.routers import DefaultRouter

from prueba.viewsets import (ConcursoViewSet, InscripcionViewSet,
                             ParticipanteViewSet)

router = DefaultRouter()

router.register("concursos", ConcursoViewSet, "concurso")
router.register("inscripciones", InscripcionViewSet, "inscripcion")
router.register("participantes", ParticipanteViewSet, "participante")
