from rest_framework_jwt.views import ObtainJSONWebToken, VerifyJSONWebToken, RefreshJSONWebToken
from django.urls import path

app_name = "jwt"

urlpatterns = [
    path("obtain-token/", ObtainJSONWebToken.as_view(), name="obtain_token"),
    path("refresh-token/", RefreshJSONWebToken.as_view(), name="refresh_token"),
    path("verify-token/", VerifyJSONWebToken.as_view(), name="verify_token"),
]
