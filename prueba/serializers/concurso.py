from django.db.models import Case, F
from django.db.models import Value as V
from django.db.models import When
from rest_framework import serializers as sz

from prueba.models import Concurso, Inscripcion


class ConcursoSerializer(sz.ModelSerializer):
    created_by = sz.HiddenField(default=sz.CurrentUserDefault())

    participantes = sz.SerializerMethodField()

    def get_participantes(self, instance):
        fields = ("email", "first_name", "last_name", "inscripcion_activa")
        participantes = (
            Inscripcion.objects.filter(concurso=instance)
            .annotate(
                email=F("participante__email"),
                first_name=F("participante__first_name"),
                last_name=F("participante__last_name"),
                inscripcion_activa=Case(
                    When(status=True, then=V("Si")),
                    When(status=False, then=V("No")),
                )
            )
            .only(*fields)
            .values(*fields)
        )

        p = list(
            map(
                lambda x: {
                    "email": x["email"],
                    "fist_name": x["first_name"],
                    "last_name": x["last_name"],
                    "inscripcion_activa": x["inscripcion_activa"],
                },
                participantes,
            )
        )
        return p

    class Meta:
        model = Concurso
        fields = (
            "id", "name", "start_date",
            "end_date", "created_by",
            "participantes"
        )

    def __init__(self, *args, **kwargs):
        fields_out = kwargs.pop("fields_out", [])
        super().__init__(*args, **kwargs)
        for field in fields_out:
            self.fields.pop(field)
