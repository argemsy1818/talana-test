import re

from rest_framework import serializers as sz

from prueba.models import Participante
from utils.normalize_text import normalize_text


class ParticipanteSerializer(sz.ModelSerializer):
    created_by = sz.HiddenField(
        default=sz.CurrentUserDefault()
    )

    class Meta:
        model = Participante
        fields = (
            "first_name",
            "last_name",
            "email",
            "created_by",
        )

    def validate_first_name(self, nombre):
        if nombre in ["", None]:
            raise sz.ValidationError(
                {
                    "first_name": "Campo nombre requerido"
                })
        nombre = normalize_text(nombre)
        return nombre

    def validate_last_name(self, apellido):
        if apellido in ["", None]:
            raise sz.ValidationError(
                {
                    "last_name": "Campo apellido requerido"
                })
        apellido = normalize_text(apellido)
        return apellido

    def validate_email(self, email):
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        if email in ["", None]:
            raise sz.ValidationError(
                {
                    "email": "Campo email requerido"
                })

        if not re.match(regex, email):
            raise sz.ValidationError(
                {"email": "Email no válido"}
            )
        return email
