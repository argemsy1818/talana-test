from rest_framework import serializers as sz

from prueba.models import Inscripcion


class InscripcionSerializer(sz.ModelSerializer):
    created_by = sz.HiddenField(
        default=sz.CurrentUserDefault()
    )

    class Meta:
        model = Inscripcion
        fields = (
            "concurso",
            "participante",
            "status",
            "created_by",
        )

    def validate(self, attrs):
        concurso = attrs.get('concurso', self.object.field1)
        participante = attrs.get('participante', self.object.field2)

        try:
            obj = Inscripcion.objects.get(
                                            concurso=concurso,
                                            participante=participante
                                        )
        except Inscripcion.DoesNotExist:
            return attrs
        if self.object and obj.id == self.object.id:
            return attrs
        else:
            raise sz.ValidationError({
                "msg": ("Este participante ya se encuentra\
                    inscrito en este concurso.")
            })

    def update(self, instance, validated_data):
        """ UPDATE, el método quedo inhabilitado para  actualizaciones. """
        pass