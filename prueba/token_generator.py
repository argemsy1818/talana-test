import jwt
from rest_framework_jwt.settings import api_settings

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class JWTokenExternalUser:
    def encode_token(
            self,
            transaction_id: str,
            project_name: str
        ) -> str:
        payload = {
            "transaction_id": transaction_id,
            # "exp": datetime.utcnow() + timedelta(hours=1),
            "iss": project_name,
        }
        token = jwt_encode_handler(payload)

        return token

    def decode_token(self, token: str):
        try:
            decode = jwt_decode_handler(token)
        except jwt.exceptions.InvalidSignatureError:
            return False
        except jwt.exceptions.ExpiredSignatureError:
            return False
        except jwt.exceptions.DecodeError:
            return False
        else:
            return decode
