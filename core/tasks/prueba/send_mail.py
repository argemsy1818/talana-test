from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.urls import reverse

from prueba.models import Inscripcion
from prueba.token_generator import JWTokenExternalUser

User = get_user_model()

logger = get_task_logger(__name__)


@shared_task
def complete_inscription(**kwargs):
    current_site = kwargs.pop("current_site")
    transaction_id = kwargs.pop("transaction_id")

    try:
        inscription = Inscripcion.objects.get(transaction_id=transaction_id)
    except Inscripcion.DoesNotExist:
        msg = f"=== NO EXISTE LA INSCRIPCION CON EL TRANSACTION ID\
            {transaction_id} ==="
        logger.info(msg)
        print(msg)
        return False
    else:

        participante = inscription.participante
        url = reverse(
            "prueba:inscripcion-activar",
            kwargs={"transaction_id": transaction_id},
        )
        form_action = f"{current_site}{url}"
        url_redirect = f"{current_site}/"
        print(form_action)
        contexto_email = {
            "py_name": settings.PY_NAME,
            "first_name": participante.first_name,
            "last_name": participante.last_name,
            "email": participante.email,
            "form_action": form_action,
            "url": url_redirect
        }

        jwt = JWTokenExternalUser()
        jwt.encode_token(str(participante.transaction_id), settings.PY_NAME)

        template = get_template("home.html")
        content = template.render(contexto_email)

        email = EmailMultiAlternatives(
            "Correo Talana!!!",
            "Talana!!!",
            settings.EMAIL_HOST_USER,
            [participante.email],
        )

        email.attach_alternative(content, "text/html")
        email.send()
        print("REVISA TU BANDEJA DE ENTRADA")
        logger.info("Enviado el mail")
        return True
