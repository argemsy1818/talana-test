from .send_mail import complete_inscription

__all__ = [
    "complete_inscription",
]