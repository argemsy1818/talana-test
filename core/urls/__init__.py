from django.urls import path

from core.views import home_index

app_name = "core"

urlpatterns = [
    path("", home_index, name="home_index")
]