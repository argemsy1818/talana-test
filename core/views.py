from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.


def home_index(request):
    py_name = "Talana"
    return render(
        request, "index.html", {"py_name": py_name}
    )